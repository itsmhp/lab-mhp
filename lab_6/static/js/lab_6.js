// chat box

var displayChat = document.getElementById('msg');

if (!localStorage.getItem("chat")) {
    localStorage.setItem("chat", JSON.stringify([]));
}
var chatHistory = JSON.parse(localStorage.getItem("chat"));


var send = document.getElementById('enter');
var i = 0;

send.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) {
        if (document.getElementById('enter').value === "") {
            alert("Please insert message")
            return
        }

        else if (document.getElementById('enter').value === "/clear") {
            displayChat.innerHTML = '';
            document.getElementById('enter').value = "";
            localStorage.clear();
            return
        }

        var message = {
            text: document.getElementById('enter').value,
            dateTime: new Date().toLocaleTimeString() + " " + new Date().toDateString(),
        };
        chatHistory.push(message);
        document.getElementById('enter').value = "";
        localStorage.setItem("chat", JSON.stringify(chatHistory));

        localData = localStorage.getItem("chat");
        localData = JSON.parse(localData);
        var templateDiv =
            "<div class='message'>" +
            "<p class='text'>" + localData[localData.length - 1].text + "</p>" + "<div class='clear'></div>" + "</div>" +
            "<p class='datetime'>" + localData[localData.length - 1].dateTime + "</p>" + "<div class='clear'></div>";
        displayChat.innerHTML += templateDiv;
    }

});

function refresh() {
    localData = localStorage.getItem("chat");
    localData = JSON.parse(localData);
    for (var i = 0; i < localData.length; i++) {
        var templateDiv =
            "<div class='message'>" +
            "<p class='text'>" + localData[i].text + "</p>" + "<div class='clear'></div>" + "</div>" +
            "<p class='datetime'>" + localData[i].dateTime + "</p>" + "<div class='clear'></div>";
        displayChat.innerHTML += templateDiv;
    }
}

$(document).ready(function(){
	console.log("hey");
	refresh();
    $("#toggle").click(function(){
        $(".chat-body").toggle(500);
    });
});
//END

// Calculator
var print = document.getElementById('print');
var erase = false;

Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};


var go = function (x) {
    if (x === 'ac') {
        print.value = null;
        erase = false;
    } else if (x === 'eval') {
        print.value = (evil(print.value));
        erase = true;
    } else if (x === 'sin' || x === 'tan') {
        print.value = (evil('Math.' + x + '(Math.radians(' + evil(print.value) + '))'));
        erase = true;
    } else if (x === 'log') {
        print.value = ((Math.log10(evil(print.value))));
        erase = true;
    } else if (erase === true) {
        print.value = x;
        erase = false;
    } else {
        print.value += x;
    }
};

function evil(fn) {
    return new Function('return ' + fn)();
}

// END

// Select2
function changeTheme(x) {
    $('body').css({"backgroundColor": x['bcgColor']});
    $('.text-center').css({"color": x['fontColor']});
}

if (localStorage.getItem('themes') === null) {
    localStorage.setItem('themes',
    [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]);
}
var themes = JSON.parse(localStorage.getItem('themes'));
if (localStorage.getItem('selectedTheme') === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(themes[3]));
}
var theme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(theme);

$(document).ready(function () {
    $('.my-select').select2({'data': themes}).val(theme['id']).change();
    $('.apply-button').on('click', function () {
        theme = themes[$('.my-select').val()];
        changeTheme(theme);
        localStorage.setItem('selectedTheme', JSON.stringify(theme));
    })
});
