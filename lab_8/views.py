from django.shortcuts import render


# Create your views here.
def index(request):
    response = {'author': "Muhammad Hanif Pratama"}
    html = "lab_8/lab_8.html"
    return render(request, html, response)
